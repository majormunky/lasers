extends StaticBody2D

signal mirror_selected(mirror)

func _on_Area2D_input_event(viewport: Node, event: InputEvent, shape_idx: int) -> void:
	if event.is_action_type():
		if event.is_action_pressed("click"):
			emit_signal("mirror_selected", self)
