extends Node2D

signal turret_selected(turret)

onready var Bullet = preload("res://Bullet.tscn")
onready var shoot_timer = $ShootTimer
var can_shoot = true
var is_shooting = false

func _process(_delta: float) -> void:
	if can_shoot && is_shooting:
		shoot(global_position + Vector2.RIGHT)

func shoot(pos: Vector2) -> void:
	var b = Bullet.instance()
	var a = (pos - global_position).angle()
	b.start(pos, a)
	get_parent().add_child(b)
	can_shoot = false
	shoot_timer.start()

func _on_ShootTimer_timeout() -> void:
	can_shoot = true

func _on_Area2D_input_event(_viewport: Node, event: InputEvent, _shape_idx: int) -> void:
	if event.is_action_type():
		if event.is_action_pressed("click"):
			emit_signal("turret_selected", self)
