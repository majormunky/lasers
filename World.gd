extends Node2D

enum {
	NORMAL,
	TURRET_PLACE,
	TURRET_SELECTED,
	MIRROR_PLACE,
	MIRROR_SELECTED,
}

onready var turret_group = $Turrets
onready var mirror_group = $Mirrors
onready var ui = $UI
onready var state_label = $UI/ColorRect/StateLabel
onready var Turret = preload("res://Turret.tscn")
onready var Mirror = preload("res://Mirror.tscn")
onready var highlight = $HighlightTile
onready var mirror_selected_buttons = $UI/ColorRect/MirrorSelectedButtons
onready var turret_selected_buttons = $UI/ColorRect/TurretSelectedButtons

var grid_size
var cell_size = 64
var state = NORMAL
var highlight_rect = null
var selected


func _ready() -> void:
	grid_size = Vector2(
		get_viewport_rect().size.x - ui.rect_size.x,
		get_viewport_rect().size.y
	)
	mirror_selected_buttons.visible = false
	set_process_input(true)
	change_state("Normal", NORMAL, false, false)


func _input(event: InputEvent) -> void:
	if state == TURRET_PLACE || state == MIRROR_PLACE:
		var pos = Vector2(
			(int(get_global_mouse_position().x/cell_size) * cell_size) + cell_size / 2,
			(int(get_global_mouse_position().y/cell_size) * cell_size) + cell_size / 2
		)
		if pos.x < 0 || pos.y < 0:
			return
		
		if pos.x > grid_size.x || pos.y > grid_size.y:
			return
		
		highlight.position = pos
		if event.is_action_type() && event.is_action_pressed("click"):
			if state == TURRET_PLACE:
				create_turret(pos)
				change_state("Normal", NORMAL, false, true)
			elif state == MIRROR_PLACE:
				create_mirror(pos)
				change_state("Normal", NORMAL, false, true)
	elif state == TURRET_SELECTED || state == MIRROR_SELECTED:
		if event.is_action_type() && event.is_action_pressed("click"):
			var grid_rect = Rect2(Vector2.ZERO, grid_size)
			if grid_rect.has_point(get_global_mouse_position()):
				remove_selected_item()
				change_state("Normal", NORMAL, false, true)


func update_selected_item(new_val: Node2D):
	selected = new_val
	highlight.position = selected.position
	highlight.visible = true
	if selected.get_parent() == mirror_group:
		mirror_selected_buttons.visible = true
		turret_selected_buttons.visible = false
	elif selected.get_parent() == turret_group:
		turret_selected_buttons.visible = true
		mirror_selected_buttons.visible = false


func remove_selected_item() -> void:
	selected = null
	turret_selected_buttons.visible = false
	mirror_selected_buttons.visible = false


func _on_turret_selected(turret: Node2D) -> void:
	if state != NORMAL:
		return
	update_selected_item(turret)
	change_state("Turret Selected", TURRET_SELECTED, true, true)


func _on_mirror_selected(mirror: Node2D) -> void:
	if state != NORMAL:
		return
	update_selected_item(mirror)
	change_state("Mirror Selected", MIRROR_SELECTED, true, false)


func create_turret(pos: Vector2) -> void:
	var new_turret = Turret.instance()
	new_turret.position = pos
	new_turret.connect("turret_selected", self, "_on_turret_selected")
	turret_group.add_child(new_turret)


func create_mirror(pos: Vector2) -> void:
	var new_mirror = Mirror.instance()
	new_mirror.position = pos
	new_mirror.connect("mirror_selected", self, "_on_mirror_selected")
	mirror_group.add_child(new_mirror)


func _draw() -> void:
	for y in range(0, grid_size.y, cell_size):
		for x in range(0, grid_size.x, cell_size):
			draw_line(Vector2(x, y), Vector2(x, y + cell_size), Color(0, 1, 0), 1.0)
			draw_line(Vector2(x, y), Vector2(x + cell_size, y), Color(0, 1, 0), 1.0)


func _on_AddTurretButton_button_up() -> void:
	change_state("Turret Place", TURRET_PLACE, true, false)


func _on_AddMirrorButton_button_up() -> void:
	change_state("Mirror Place", MIRROR_PLACE, true, false)


func change_state(label: String, state_val: int, highlight_val: bool, delay_state_change: bool) -> void:
	state_label.bbcode_text = "State: " + label
	highlight.visible = highlight_val
	if delay_state_change:
		yield(get_tree().create_timer(0.1), "timeout")
		state = state_val
	else:
		state = state_val


func _on_RotateRightButton_button_up() -> void:
	selected.rotation_degrees += 90


func _on_RotateLeftButton_button_up() -> void:
	selected.rotation_degrees -= 90


func _on_ToggleShootButton_button_up() -> void:
	selected.is_shooting = !selected.is_shooting


func _on_RemoveTurretButton_button_up() -> void:
	selected.queue_free()
	remove_selected_item()
	change_state("Normal", NORMAL, false, false)


func _on_RemoveMirrorButton_button_up() -> void:
	selected.queue_free()
	remove_selected_item()
	change_state("Normal", NORMAL, false, false)
