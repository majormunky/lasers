# Lasers
Prototype game built with Godot.  You can place a gun tower that shoots a ball of light that can bounce off of the mirrors.

Artwork from [Kenney](https://kenney.nl)
