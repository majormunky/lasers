extends KinematicBody2D

var speed = 1000.0
var velocity = Vector2()

func start(pos: Vector2, dir: float) -> void:
	position = pos
	rotation = dir
	velocity = Vector2(speed, 0.0).rotated(dir)

func _physics_process(delta: float) -> void:
	var collision = move_and_collide(velocity * delta)
	if collision:
		velocity = velocity.bounce(collision.normal)
		if collision.collider.has_method("hit"):
			collision.collider.hit()

func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
